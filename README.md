Author: Mateusz Wasiak

Topic: Windmill

Project for GKOM (Computer Graphics) lessons on WUT.

Implementation: C++ with OpenGL

The user has the ability to navigate the created world

Results:

![Alt text](view1.png?raw=true "view1")
![Alt text](view2.png?raw=true "view2")
![Alt text](view3.png?raw=true "view3")
![Alt text](view4.png?raw=true "view4")
![Alt text](view5.png?raw=true "view5")
![Alt text](view6.png?raw=true "view6")
![Alt text](view7.png?raw=true "view7")